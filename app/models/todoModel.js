1//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema
const Schema = mongoose.Schema;

const todoSchema = new Schema ({
    userId : {
        type : mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    title: {
        type: String,
        required : true
    },
    completed: {
        type: Boolean,
        default: 0
    }
})
module.exports =mongoose.model("todo", todoSchema)