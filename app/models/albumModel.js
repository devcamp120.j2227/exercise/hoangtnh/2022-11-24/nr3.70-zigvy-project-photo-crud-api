//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user",
    },
    title: {
        type: String,
        require: true
    },
    photos: [{
        type: mongoose.Types.ObjectId,
        ref: "photo"
    }]
})
module.exports = mongoose.model("album", AlbumSchema)