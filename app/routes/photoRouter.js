const express = require("express");
const router = express.Router();
const photoController = require("../controllers/photoController");

router.post("/photos", photoController.createPhoto);
//router.get("/photos", photoController.getAllPhoto);
router.put("/photos/:photoId", photoController.updatePhoto);
router.get("/photos/:photoId", photoController.getPhotoById);
router.delete("/photos/:photoId", photoController.deletePhoto);
router.post("/albums/:albumId/photos", photoController.createPhotoToAlbumId);
router.get("/albums/:albumId/photos", photoController.getAllPhotoOfAlbum);
router.get("/photos", photoController.getAllPhotoByQuery);

module.exports = router;