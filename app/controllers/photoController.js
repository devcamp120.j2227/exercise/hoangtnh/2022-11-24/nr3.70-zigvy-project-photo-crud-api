const mongoose = require("mongoose");
const photoModel = require("../models/photoModel");
const albumModel = require("../models/albumModel");

//function create photo
const createPhoto = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    if(!body.url ){
        return response.status(400).json({
            status:"Bad request",
            message:"Url is not valid"
        })
    } 
    if(!body.thumbnailUrl ){
        return response.status(400).json({
            status:"Bad request",
            message:"Thumbnail url is not valid"
        })
    } 
   
    //B3: thao tác với CSDL
    let newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId : mongoose.Types.ObjectId(),
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    if(body.title !== undefined){
        newPhoto.title = body.title
    }
    if(body.url !== undefined){
        newPhoto.url = body.url
    }
    if(body.thumbnailUrl !== undefined){
        newPhoto.thumbnailUrl = body.thumbnailUrl
    }
    photoModel.create(newPhoto, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new photo successfully",
            data: data
        })
    })
}

//function get all photo
const getAllPhoto = (request, response) => {
    photoModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all Photos successfully",
            data: data
        })
    })
}
//function update photo by id
const updatePhoto = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let photoId = request.params.photoId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return response.status(400).json({
            status:"Bad request",
            message:" Photo id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    if(!body.url ){
        return response.status(400).json({
            status:"Bad request",
            message:"Url is not valid"
        })
    } 
    if(!body.thumbnailUrl ){
        return response.status(400).json({
            status:"Bad request",
            message:"Thumbnail url is not valid"
        })
    } 
    //B3: gọi model thao tác CSDL
    const photoUpdate = {};
    if(body.title !== undefined){
        photoUpdate.title = body.title
    }
    if(body.url !== undefined){
        photoUpdate.url = body.url
    }
    if(body.thumbnailUrl !== undefined){
        photoUpdate.thumbnailUrl = body.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(photoId, photoUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update photo with id${photoId} successfully`,
            data: data
        })
    })
}
//function get photo by id
const getPhotoById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let photoId = request.params.photoId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Photo id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    photoModel.findById(photoId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get Photo by id ${photoId} successfully`,
            data: data
        })
    })
}
//function delete photo by id
const deletePhoto = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let photoId = request.params.photoId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Photo id is not valid"
        })
    }
    //B3: thao tác với CSDL
    photoModel.findByIdAndRemove(photoId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted photo by id ${photoId} successfully`
        })
    })
}
//function create photo and push to album via photos obj
const createPhotoToAlbumId = (request, response) => {
    //B1 chuẩn bị dữ liệu
    const albumId = request.params.albumId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return response.status(400).json({
            status:"Bad request",
            message: " Album id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    if(!body.url ){
        return response.status(400).json({
            status:"Bad request",
            message:"Url is not valid"
        })
    } 
    if(!body.thumbnailUrl ){
        return response.status(400).json({
            status:"Bad request",
            message:"Thumbnail url is not valid"
        })
    } 
    //B3: thao tác với CSDL
    let newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId : albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
   
    photoModel.create(newPhoto,(error, data) =>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //Thêm Id của photo mới vào mảng photos trong album
        albumModel.findByIdAndUpdate(albumId, {
            $push: {
                photos: data._id
            }
        }, (err,updateAlbum) =>{
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status:"Create photo successfully",
                data: data
            })
        })
    })
}
//function get all photo of album
const getAllPhotoOfAlbum = (request, response) =>{
    const albumId = request.params.albumId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Album ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    albumModel.findById(albumId)
        .populate("photos")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all photos of album successfully",
                data: data
            })
        })
        console.log(albumId)
}
//function get all photo by album id using query
const getAllPhotoByQuery = (request, response) =>{
    const albumId = request.query.albumId;
    //nếu truyền vào user Id query
    if(albumId){
        if(!mongoose.Types.ObjectId.isValid(albumId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        }
        albumModel.findById(albumId)
            .populate("photos")
            .exec((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get photos by albumId successfully",
                data: data.photos

            })
        })
    }
    //nếu không truyền vào user id
    else{
        photoModel.find((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all photos successfully",
                data: data

            })
        })
    }
    
}
module.exports = {
    createPhoto,
    //getAllPhoto,
    updatePhoto,
    getPhotoById,
    deletePhoto,
    createPhotoToAlbumId,
    getAllPhotoOfAlbum,
    getAllPhotoByQuery
}